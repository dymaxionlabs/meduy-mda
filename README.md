# meduy-mda

Este repositorio contiene Jupyter Notebooks con ejemplos del uso del paquete
`meduy-ml` para el proyecto Monitoreo de Asentamientos.

## Instalación

Es necesario tener instalado el paquete [meduy-ml](https://gitlab.com/dymaxionlabs/meduy-ml).
Por favor seguir las [instrucciones de instalación](https://gitlab.com/dymaxionlabs/meduy-ml/blob/master/README.md#instalaci%C3%B3n).

Para abrir los notebooks es necesario tener Jupyter instalado.  Primero active
el entorno virtual donde tenga instalado `meduy-ml` y ejecute:

```bash
pip install jupyter
```

## Uso

Sólo es necesario ejecutar Jupyter para abrir los notebooks contenidos en este
repositorio.  Habiendo activado el entorno donde está instalado `meduy-ml`,
volver al directorio clonado de este repositorio y ejecutar:

```bash
jupyter notebook
```

## Notebooks

Hay diferentes notebooks para cada etapa de procesamiento.

Para entrenar un modelo, seguir los pasos descriptos de los notebooks 1 y 2.

Una vez que se tenga un modelo entrenado, para realizar una predicción nueva,
seguir los pasos 1, 3, y 4.

1. [Descarga de imágenes](1_Descarga_de_imagenes.ipynb)
2. [Entrenamiento](2_Entrenamiento.ipynb)
3. [Predicción](3_Prediccion.ipynb)
4. [Post-procesamiento](4_Post-procesamiento.ipynb)
